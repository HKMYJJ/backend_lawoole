<?php

namespace App\Http\Controllers\ZXZ;

use Lawoole\Contracts\Foundation\Application;
use Lawoole\Routing\Controller;

class IndexController extends Controller
{
    /**
     * 首页
     *
     * @param \Lawoole\Contracts\Foundation\Application $app
     *
     * @return mixed
     */
    public function index(Application $app)
    {
        $request = $app->request;
        $redirect = $app->redirect;
        $param = $request->all();
        var_dump($param);
        return "资讯站";
    }
}
