<?php

return [

    /*
    |--------------------------------------------------------------------------
    | 中间件定义
    |--------------------------------------------------------------------------
    */

    'middleware' => [

    ],

    /*
    |--------------------------------------------------------------------------
    | 路由中间件定义
    |--------------------------------------------------------------------------
    */

    'route_middleware' => [

    ],

    /*
    |--------------------------------------------------------------------------
    | 路由组定义
    |--------------------------------------------------------------------------
    */

    'middleware_groups' => [

    ],

    /*
    |--------------------------------------------------------------------------
    | 控制器根命名空间
    |--------------------------------------------------------------------------
    */

    'namespace' => 'App\Http\Controllers',

    /*
    |--------------------------------------------------------------------------
    | 路由规则文件
    |--------------------------------------------------------------------------
    */

    'routes' => [

        'web' => [
            'path' => route_path('web.php'),
        ],
        'admin' => [
            'path'       => route_path('admin.php'),
            'prefix'     => 'admin',
            'middleware' => [
                Lawoole\Foundation\Http\Middleware\ParseJsonRequest::class
            ]
        ],
        'jyz' => [
            'path'       => route_path('jyz.php'),
            'prefix'     => 'jyz',
            'middleware' => [
                Lawoole\Foundation\Http\Middleware\ParseJsonRequest::class
            ]
        ],
        'myz' => [
            'path'       => route_path('myz.php'),
            'prefix'     => 'myz',
            'middleware' => [
                Lawoole\Foundation\Http\Middleware\ParseJsonRequest::class
            ]
        ],
        'xyz' => [
            'path'       => route_path('xyz.php'),
            'prefix'     => 'xyz',
            'middleware' => [
                Lawoole\Foundation\Http\Middleware\ParseJsonRequest::class
            ]
        ],
        'zxz' => [
            'path'       => route_path('zxz.php'),
            'prefix'     => 'zxz',
            'middleware' => [
                Lawoole\Foundation\Http\Middleware\ParseJsonRequest::class
            ]
        ]
    ]

];
