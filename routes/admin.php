<?php

/*
|--------------------------------------------------------------------------
| 路由规则
|--------------------------------------------------------------------------
*/

$router->any('', 'Admin\IndexController@index');
$router->get("/{foobar}", function ($foobar) use ($router) {
    return "/" . $foobar;
});